package com.example.android.camera2basic;

public class CameraState {

    public enum Mode {
        AUTO,
        VIDEO,
        MANUAL
    }

    public enum Flash {
        ON,
        OFF,
        AUTO
    }

    public enum Timer {
        OFF,
        THREE,
        TEN
    }

    public enum Grid {
        OFF,
        THIRD,
        FOUR
    }

    public enum WhiteBalance {
        AUTO,
        SUN,
        CLOUD,
        IRI,
        INC,
    }

    public enum Filter {
        BLACKWHITE,
        VINTAGE,
        SEPIA,
        NEGATIVE,
        OFF
    }

    public enum Face {
        BACK,
        FRONT
    }

    public enum Format{
        JPEG,
        RAW
    }

    private Mode mode = Mode.AUTO;
    private Flash flash = Flash.AUTO;
    private Timer timer = Timer.OFF;
    private Grid grid = Grid.OFF;
    private WhiteBalance whiteBalance = WhiteBalance.AUTO;
    private Filter filter = Filter.OFF;
    private Face face = Face.BACK;
    private Format format = Format.JPEG;

    public CameraState() {}

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Flash getFlash() {
        return flash;
    }

    public void setFlash(Flash flash) {
        this.flash = flash;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public WhiteBalance getWhiteBalance() {
        return whiteBalance;
    }

    public void setWhiteBalance(WhiteBalance whiteBalance) {
        this.whiteBalance = whiteBalance;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public Filter getFilter() {
        return this.filter;
    }

    public Face getFace() {
        return face;
    }

    public void setFace(Face face) { this.face = face; }

    public Format getFormat(){ return format;}

    public void setFormat(Format format){this.format = format;}
}
