package com.example.android.camera2basic;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SettingsFragment extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String[] options = {"Resolution","Timer","Credits","Manual"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(), android.R.layout.simple_list_item_1, options);
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id){
        super.onListItemClick(listView, view, position, id);
        if(position == 0){
            String[] options = {"1440p", "1080p", "720p", "480p"};
            makeDialogBox(options);
            Toast.makeText(getContext(), "Resolution", Toast.LENGTH_LONG).show();
        }else if(position == 1){
            String[] options = {"1s", "3s", "5s", "10s"};
            makeDialogBox(options);
            Toast.makeText(getContext(), "Timer", Toast.LENGTH_LONG).show();
        }else if(position == 2){
            Toast.makeText(getContext(), "Credits", Toast.LENGTH_LONG).show();
        }else if(position == 3){
            Toast.makeText(getContext(), "Manual", Toast.LENGTH_LONG).show();
        }
    }

    private void makeDialogBox(final String[] menuOptions){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Select an option");
        builder.setItems(menuOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int clickedItem) {
                // the user clicked on
                if(clickedItem == 0){
                    String selected = menuOptions[0];
                    Toast.makeText(getContext(), selected, Toast.LENGTH_LONG).show();
                }else if(clickedItem == 1){
                    String selected = menuOptions[1];
                    Toast.makeText(getContext(), selected, Toast.LENGTH_LONG).show();
                }else if(clickedItem == 2){
                    String selected = menuOptions[2];
                    Toast.makeText(getContext(), selected, Toast.LENGTH_LONG).show();
                }else if(clickedItem == 3){
                    String selected = menuOptions[3];
                    Toast.makeText(getContext(), selected, Toast.LENGTH_LONG).show();
                }else{
                    ;
                }
            }
        });
        builder.show();
    }
}
