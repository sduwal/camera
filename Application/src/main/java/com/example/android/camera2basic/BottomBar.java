package com.example.android.camera2basic;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Range;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
/**
 * A simple {@link Fragment} subclass.
 */
public class BottomBar extends Fragment implements View.OnClickListener {

    private Button autoButton;
    private Button videoButton;
    private Button manualButton;
    private ImageButton galleryButton;
    private CameraState.Mode modeState;
    private LinearLayout manualPanel;

    private ImageButton flipButton;
    private ImageButton filterButton;
    private Button formatButton;

    private SeekBar focusbar;
    private SeekBar shutterbar;
    private SeekBar isobar;


    private int focusProgress;
    private long shutterProgress;
    private int isoProgress;


    private CameraState cameraState;
    Camera2BasicFragment camera2BasicFragment;
    private CameraManager manager;
    private CaptureRequest mPreviewRequest;
    private CameraCaptureSession mCaptureSession;
    private String mCameraId;

    private Integer facing;

    public static float focusValue;
    public static int isoValue;
    public static long shutterValue;

    public BottomBar() {
        // Required empty public constructor
    }

    public static BottomBar newInstance() {
        return new BottomBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_bar, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        autoButton = view.findViewById(R.id.autoButton);
        videoButton = view.findViewById(R.id.videoButton);
        manualButton = view.findViewById(R.id.manualButton);
        galleryButton = view.findViewById(R.id.galleryButton);
        manualPanel = view.findViewById(R.id.manualPanel);
        flipButton = view.findViewById(R.id.changeCameraButton);
        cameraState = ((CameraActivity) getActivity()).getState();
        modeState = cameraState.getMode();
        filterButton = getActivity().findViewById(R.id.filterButton);
        formatButton = getActivity().findViewById(R.id.format);

        setUpMode();

        focusbar = getView().findViewById(R.id.focusSeekbar);
        shutterbar = getView().findViewById(R.id.ssSeekBar);
        isobar = getView().findViewById(R.id.isoSeekBar);

        autoButton.setOnClickListener(this);
        manualButton.setOnClickListener(this);
        videoButton.setOnClickListener(this);
        flipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cameraState.getFace() == CameraState.Face.FRONT) {
                    cameraState.setFace(CameraState.Face.BACK);
                    facing = CameraCharacteristics.LENS_FACING_BACK;
                } else {
                    cameraState.setFace(CameraState.Face.FRONT);
                    facing = CameraCharacteristics.LENS_FACING_FRONT;
                }
                camera2BasicFragment.mCameraLensFacingDirection = facing;
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
            }
        });

        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setType("image/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        FragmentManager fm = getFragmentManager();
        camera2BasicFragment = (Camera2BasicFragment) fm.findFragmentById(R.id.container);
        manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing == CameraCharacteristics.LENS_FACING_FRONT) {
                    facing = CameraCharacteristics.LENS_FACING_FRONT;
                    cameraState.setFace(CameraState.Face.FRONT);
                } else {
                    facing = CameraCharacteristics.LENS_FACING_BACK;
                    cameraState.setFace(CameraState.Face.BACK);
                }
                mCameraId = cameraId;
                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.autoButton:
                switchMode(CameraState.Mode.AUTO);
                camera2BasicFragment.createCameraPreviewSession();
                break;
            case R.id.videoButton:
                switchMode(CameraState.Mode.VIDEO);
                break;
            case R.id.manualButton:
                switchMode(CameraState.Mode.MANUAL);
                camera2BasicFragment.createCameraPreviewSession();
                break;
        }
    }

    public void setUpMode() {
        modeState = CameraState.Mode.AUTO;
        changeHighlight(this.autoButton, true);
        changeHighlight(this.videoButton, false);
        changeHighlight(this.manualButton, false);
        manualPanel.setVisibility(View.INVISIBLE);
        manualPanel.setTranslationY(manualPanel.getHeight());
        flipTopButtons(false);
    }

    public void flipTopButtons(boolean isFomrat) {
        if (isFomrat) {
            ((TopBar) (getActivity().getSupportFragmentManager().findFragmentById(R.id.topContainer))).hideActiveBar();
            filterButton.animate()
                    .alpha(0.0f)
                    .setDuration(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            // don't remove this otherwise the animation wont't stick
                            filterButton.setVisibility(View.GONE);
                            formatButton.setVisibility(View.VISIBLE);
                            filterButton.setAlpha(1.0f);
                        }
                    });
        } else {
            formatButton.animate()
                    .alpha(0.0f)
                    .setDuration(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            // don't remove this otherwise the animation wont't stick
                            filterButton.setVisibility(View.VISIBLE);
                            formatButton.setVisibility(View.GONE);
                            formatButton.setAlpha(1.0f);
                        }
                    });
        }
    }

    public void switchMode(CameraState.Mode mode) {
        if (mode == CameraState.Mode.AUTO && modeState != CameraState.Mode.AUTO) {
            changeHighlight(this.autoButton, true);
            changeHighlight(this.videoButton, false);
            changeHighlight(this.manualButton, false);
            manualPanel.animate()
                    .translationY(manualPanel.getHeight())
                    .setDuration(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            manualPanel.setVisibility(View.GONE);
                        }
                    });

            flipTopButtons(false);
            this.modeState = CameraState.Mode.AUTO;
            cameraState.setMode(CameraState.Mode.AUTO);
        } else if (mode == CameraState.Mode.VIDEO && modeState != CameraState.Mode.VIDEO) {
            changeHighlight(this.videoButton, true);
            changeHighlight(this.autoButton, false);
            changeHighlight(this.manualButton, false);
            this.modeState = CameraState.Mode.VIDEO;
            cameraState.setMode(CameraState.Mode.VIDEO);
        } else if (mode == CameraState.Mode.MANUAL && modeState != CameraState.Mode.MANUAL) {
            changeHighlight(this.manualButton, true);
            changeHighlight(this.videoButton, false);
            changeHighlight(this.autoButton, false);
            manualPanel.setVisibility(View.VISIBLE);
            manualPanel.setTranslationY(manualPanel.getHeight());
            manualPanel.animate()
                    .translationY(0)
                    .setDuration(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            manualPanel.setTranslationY(0);
                        }
                    });
            flipTopButtons(true);
            this.modeState = CameraState.Mode.MANUAL;

            seekChange(focusbar);
            seekChange(isobar);
            seekChange(shutterbar);

            cameraState.setMode(CameraState.Mode.MANUAL);
        }
    }

    private void changeHighlight(Button button, boolean isHighlighted) {
        if (isHighlighted) {
            button.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_pressed));
            button.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        } else {
            button.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_disabled));
            button.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        }
    }

    // link up values to be controlled by seekbars in manual mode
    private void seekbar_init(int bar) {
        if (modeState == CameraState.Mode.MANUAL) {
            try {
                camera2BasicFragment.mPreviewRequestBuilder.set(CaptureRequest.CONTROL_MODE,
                        CaptureRequest.CONTROL_MODE_OFF);
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraId);
                mPreviewRequest = camera2BasicFragment.mPreviewRequest;
                mCaptureSession = camera2BasicFragment.mCaptureSession;


                switch (bar) {
                    case 1:
                        camera2BasicFragment.mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                CaptureRequest.CONTROL_AF_MODE_OFF);
                        float max = characteristics.get(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE);
                        focusbar.setMax((int) max);
                        float focus = max - focusProgress;
                        camera2BasicFragment.mPreviewRequestBuilder.set(CaptureRequest
                                .LENS_FOCUS_DISTANCE, focus);
                        mPreviewRequest = camera2BasicFragment.mPreviewRequestBuilder.build();
                        mCaptureSession.setRepeatingRequest(mPreviewRequest, camera2BasicFragment
                                .mCaptureCallback, camera2BasicFragment.mBackgroundHandler);
                        focusValue = focus;
                        break;

                    case 2:
                        Range<Long> rangeShutter = characteristics.get(CameraCharacteristics
                                .SENSOR_INFO_EXPOSURE_TIME_RANGE);
                        long minS = rangeShutter.getLower();
                        shutterbar.setMax(1000000000);
                        long shutter = shutterProgress;
                        camera2BasicFragment.mPreviewRequestBuilder.set(CaptureRequest.SENSOR_EXPOSURE_TIME,
                                shutter);
                        mPreviewRequest = camera2BasicFragment.mPreviewRequestBuilder.build();
                        mCaptureSession.setRepeatingRequest(mPreviewRequest, camera2BasicFragment
                                .mCaptureCallback, camera2BasicFragment.mBackgroundHandler);
                        shutterValue = shutter;
                        break;

                    case 3:
                        Range<Integer> rangeIso = characteristics.get(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE);
                        int maxI = rangeIso.getUpper();
                        int minI = rangeIso.getLower();
                        isobar.setMax(maxI);
                        int iso = isoProgress;
                        camera2BasicFragment.mPreviewRequestBuilder.set(CaptureRequest
                                .SENSOR_SENSITIVITY, iso);

                        mPreviewRequest = camera2BasicFragment.mPreviewRequestBuilder.build();
                        mCaptureSession.setRepeatingRequest(mPreviewRequest, camera2BasicFragment
                                .mCaptureCallback, camera2BasicFragment.mBackgroundHandler);
                        isoValue = iso;
                        break;
                }

            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    // monitor seekbar changes
    public void seekChange(SeekBar sk) {
        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (seekBar.equals(focusbar)) {
                    focusProgress = progress;
                    seekbar_init(1);
                } else if (seekBar.equals(shutterbar)) {
                    shutterProgress = progress;
                    seekbar_init(2);
                } else if (seekBar.equals(isobar)) {
                    isoProgress = progress;
                    seekbar_init(3);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}