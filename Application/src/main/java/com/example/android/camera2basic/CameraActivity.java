/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.camera2basic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.widget.ImageButton;

public class CameraActivity extends AppCompatActivity {

    private static ImageButton settingButton;
    private CameraState state = new CameraState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        setContentView(R.layout.activity_camera);
        if (null == savedInstanceState) {
               getSupportFragmentManager().beginTransaction()
                    .replace(R.id.topContainer,TopBar.newInstance())
                    .replace(R.id.bottomContainer,BottomBar.newInstance())
                    .replace(R.id.container, Camera2BasicFragment.newInstance())
                    .commit();
        }
    }

    protected void onResume() {
        super.onResume();
    }

    public CameraState getState() {
        return this.state;
    }


}
