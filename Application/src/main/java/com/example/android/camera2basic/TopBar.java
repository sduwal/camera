package com.example.android.camera2basic;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopBar extends Fragment {

    private static SQLiteDatabase db;
    private static Cursor cursor;

    private ImageButton settingButton;
    private ImageButton flashButton;
    private ImageButton gridButton;
    private ImageButton whiteBalanceButton;
    private ImageButton filterButton;
    private ImageButton timerButton;
    private Button formatButton;

    private LinearLayout timerBar;
    private LinearLayout flashBar;
    private LinearLayout gridBar;
    private LinearLayout whiteBalanceBar;
    private LinearLayout filterBar;
    private LinearLayout activeBar;

    private ImageButton timerOff;
    private ImageButton timer3sec;
    private ImageButton timer10sec;

    private ImageButton gridOff;
    private Button grid3;
    private Button grid4;

    private ImageButton flashOff;
    private ImageButton flashOn;
    private ImageButton flashAuto;

    private Button wbSunny;
    private Button wbCloudy;
    private Button wbIrid;
    private Button wbInc;
    private Button wbAuto;

    private Button filterBW;
    private Button filterVintage;
    private Button filterSepia;
    private Button filterNegative;
    private Button filterOff;
    public CameraState cameraState;


    public TopBar() {
        // Required empty public constructor
    }

    public static TopBar newInstance() {
        return new TopBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_top_bar, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        settingButton = view.findViewById(R.id.settingButton);
        timerButton = view.findViewById(R.id.timerButton);
        gridButton = view.findViewById(R.id.gridButton);
        flashButton = view.findViewById(R.id.flashButton);
        whiteBalanceButton = view.findViewById(R.id.whiteBalanceButton);
        filterButton = view.findViewById(R.id.filterButton);
        formatButton = view.findViewById(R.id.format);

        cameraState = ((CameraActivity) getActivity()).getState();

        final SQLiteOpenHelper helper = new OpenerSetting(getActivity());
        try {
            Log.d("Database", "first");
            db = helper.getWritableDatabase();
            cursor = db.rawQuery("SELECT TIMER,GRID,FLASH FROM OPENERSETTING", null);
        } catch (Exception e) {
            Log.d("Database", "Not Available");
        }
        Log.d("Cursor", "" + cursor.getCount());
        cursor.moveToFirst();
        cameraState.setFlash(CameraState.Flash.values()[cursor.getInt(2)]);
        cameraState.setGrid(CameraState.Grid.values()[cursor.getInt(1)]);
        cameraState.setTimer(CameraState.Timer.values()[cursor.getInt(0)]);

        timerBar = view.findViewById(R.id.timerBar);
        gridBar = view.findViewById(R.id.gridBar);
        flashBar = view.findViewById(R.id.flashBar);
        whiteBalanceBar = view.findViewById(R.id.whiteBalanceBar);
        filterBar = view.findViewById(R.id.filterBar);

        timer3sec = view.findViewById(R.id.timer3secButton);
        timer10sec = view.findViewById(R.id.timer10secButton);
        timerOff = view.findViewById(R.id.timerOffButton);

        gridOff = view.findViewById(R.id.gridOff);
        grid3 = view.findViewById(R.id.grid3);
        grid4 = view.findViewById(R.id.grid4);

        flashOff = view.findViewById(R.id.flashOff);
        flashOn = view.findViewById(R.id.flashOn);
        flashAuto = view.findViewById(R.id.flashAuto);

        wbSunny = view.findViewById(R.id.wbSunny);
        wbCloudy = view.findViewById(R.id.wbCloudy);
        wbIrid = view.findViewById(R.id.wbIrid);
        wbInc = view.findViewById(R.id.wbInc);
        wbAuto = view.findViewById(R.id.wbAuto);

        filterBW = view.findViewById(R.id.filterBW);
        filterVintage = view.findViewById(R.id.filterVintage);
        filterSepia = view.findViewById(R.id.filterSepia);
        filterNegative = view.findViewById(R.id.filterNegative);
        filterOff = view.findViewById(R.id.filterOff);

        switch (cameraState.getGrid()) {
            case OFF:
//                gridOff.callOnClick();
                gridButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_grid_off_black_24dp));
                break;
            case FOUR:
                gridButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_grid_on_black_24dp));
//                grid4.callOnClick();
                break;
            case THIRD:
                gridButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_grid_on_black_24dp));
//                grid3.callOnClick();
                break;
        }
        switch (cameraState.getFlash()) {
            case AUTO:
                flashButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_flash_auto_black_24dp));
                break;
            case ON:
                flashButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_flash_on_black_24dp));
                break;
            case OFF:
                flashButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_flash_on_black_24dp));
                break;
        }

        switch (cameraState.getTimer()) {
            case OFF:
                timerButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_timer_off_black_24dp));
                break;

            case TEN:
                timerButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_timer_10_black_24dp));
                break;

            case THREE:
                timerButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_timer_3_black_24dp));
                break;
        }

        addEvents();
        pullBars();
    }

    private void pullBars() {
        timerBar.setVisibility(View.INVISIBLE);
        gridBar.setVisibility(View.INVISIBLE);
        flashBar.setVisibility(View.INVISIBLE);
        whiteBalanceBar.setVisibility(View.INVISIBLE);
        filterBar.setVisibility(View.INVISIBLE);
        timerBar.setTranslationY(-timerBar.getHeight());
        gridBar.setTranslationY(-gridBar.getHeight());
        flashBar.setTranslationY(-flashBar.getHeight());
        whiteBalanceBar.setTranslationY(-whiteBalanceBar.getHeight());
        filterBar.setTranslationY(-filterBar.getHeight());
    }

    private void addEvents() {
        addBarEvents();
        addTimerEvents();
        addGridEvents();
        addFlashEvents();
        addWBEvent();
        addFilterEvent();
    }

    private void addBarEvents() {
        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Setting.class);
                startActivity(intent);
            }
        });
        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateBar(timerButton, timerBar);
            }
        });

        gridButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateBar(gridButton, gridBar);
            }
        });
        flashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateBar(flashButton, flashBar);
            }
        });
        whiteBalanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateBar(whiteBalanceButton, whiteBalanceBar);
            }
        });
        formatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formatButton.animate()
                        .alpha(0.0f)
                        .setDuration(100)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                // don't remove this otherwise the animation wont't stick
                                formatButton.setAlpha(1.0f);
                                if(formatButton.getText().equals("JPEG")) {
                                    formatButton.setText("RAW");
                                    cameraState.setFormat(CameraState.Format.RAW);
                                } else{
                                    formatButton.setText("JPEG");
                                    cameraState.setFormat(CameraState.Format.JPEG);
                                }
                            }
                        });

            }
        });
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateBar(filterButton, filterBar);
            }
        });
    }

    private void activateBar(View view, LinearLayout layout) {
        boolean animate = true;
        if (activeBar != null) {
            if (activeBar == layout && activeBar.getVisibility() == View.VISIBLE) {
                animate = false;
                hideActiveBar();
            } else {
                activeBar.setVisibility(View.GONE);
            }
        }
        if (animate) {
            activeBar = layout;
            ((Overlay) getActivity().findViewById(R.id.surfaceView)).drawGrid(cameraState.getGrid());
            setButtonsAlpha(0.5f);
            view.setAlpha(1.0f);
            activeBar.setVisibility(View.VISIBLE);
            activeBar.setTranslationY(-activeBar.getHeight());
            activeBar.animate()
                    .translationY(0)
                    .setDuration(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            // don't remove this otherwise the animation wont't stick
                        }
                    });
        }
    }

    private void setButtonsAlpha(float val) {
        settingButton.setAlpha(val);
        timerButton.setAlpha(val);
        gridButton.setAlpha(val);
        flashButton.setAlpha(val);
        whiteBalanceButton.setAlpha(val);
        filterButton.setAlpha(val);
    }

    private void addTimerEvents() {
        timer3sec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_timer_3_black_24dp));
                cameraState.setTimer(CameraState.Timer.THREE);
                db.execSQL("UPDATE OPENERSETTING SET TIMER=1;");
                hideActiveBar();
            }
        });

        timer10sec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_timer_10_black_24dp));
                cameraState.setTimer(CameraState.Timer.TEN);
                db.execSQL("UPDATE OPENERSETTING SET TIMER=2;");
                hideActiveBar();
            }
        });
        timerOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_timer_off_black_24dp));
                cameraState.setTimer(CameraState.Timer.OFF);
                db.execSQL("UPDATE OPENERSETTING SET TIMER=0;");
                hideActiveBar();
            }
        });
    }

    private void addGridEvents() {
        grid3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_grid_on_black_24dp));
                cameraState.setGrid(CameraState.Grid.THIRD);
                db.execSQL("UPDATE OPENERSETTING SET GRID=1;");
                ((Overlay) getActivity().findViewById(R.id.surfaceView)).drawGrid(cameraState.getGrid());
                hideActiveBar();
            }
        });
        grid4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_grid_on_black_24dp));
                cameraState.setGrid(CameraState.Grid.FOUR);
                db.execSQL("UPDATE OPENERSETTING SET GRID=2;");
                ((Overlay) getActivity().findViewById(R.id.surfaceView)).drawGrid(cameraState.getGrid());
                hideActiveBar();
            }
        });
        gridOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_grid_off_black_24dp));
                cameraState.setGrid(CameraState.Grid.OFF);
                db.execSQL("UPDATE OPENERSETTING SET GRID=0;");
                ((Overlay) getActivity().findViewById(R.id.surfaceView)).drawGrid(cameraState.getGrid());
                hideActiveBar();
            }
        });

    }

    private void addFlashEvents() {
        flashOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_flash_on_black_24dp));
                cameraState.setFlash(CameraState.Flash.ON);
                db.execSQL("UPDATE OPENERSETTING SET FLASH=0;");
                hideActiveBar();
            }
        });

        flashOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_flash_off_black_24dp));
                cameraState.setFlash(CameraState.Flash.OFF);
                db.execSQL("UPDATE OPENERSETTING SET FLASH=1;");
                hideActiveBar();
            }
        });

        flashAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_flash_auto_black_24dp));
                cameraState.setFlash(CameraState.Flash.AUTO);
                db.execSQL("UPDATE OPENERSETTING SET FLASH=2;");
                hideActiveBar();
            }
        });

    }

    private void addWBEvent() {
        final Camera2BasicFragment camera2BasicFragment = (Camera2BasicFragment) getFragmentManager().findFragmentById(R.id.container);
        wbSunny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whiteBalanceButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_wb_sunny_black_24dp));
                cameraState.setWhiteBalance(CameraState.WhiteBalance.SUN);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });

        wbCloudy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whiteBalanceButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_wb_cloudy_black_24dp));
                cameraState.setWhiteBalance(CameraState.WhiteBalance.CLOUD);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });

        wbIrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whiteBalanceButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_wb_iridescent_black_24dp));
                cameraState.setWhiteBalance(CameraState.WhiteBalance.IRI);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });

        wbInc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whiteBalanceButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_wb_incandescent_black_24dp));
                cameraState.setWhiteBalance(CameraState.WhiteBalance.INC);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });

        wbAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whiteBalanceButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_wb_auto_black_24dp));
                cameraState.setWhiteBalance(CameraState.WhiteBalance.AUTO);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });
    }

    private void addFilterEvent() {
        final Camera2BasicFragment camera2BasicFragment = (Camera2BasicFragment) getFragmentManager().findFragmentById(R.id.container);
        filterBW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_filter_b_and_w_black_24dp));
                cameraState.setFilter(CameraState.Filter.BLACKWHITE);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });

        filterVintage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_gradient_black_24dp));
                cameraState.setFilter(CameraState.Filter.VINTAGE);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });

        filterSepia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_filter_vintage_black_24dp));
                cameraState.setFilter(CameraState.Filter.SEPIA);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });

        filterNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_compare_black_24dp));
                cameraState.setFilter(CameraState.Filter.NEGATIVE);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });

        filterOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_filter_none_black_24dp));
                cameraState.setFilter(CameraState.Filter.OFF);
                camera2BasicFragment.closeCamera();
                camera2BasicFragment.onResume();
                hideActiveBar();
            }
        });
    }

    private void addBarOptionsEvents() {
        flashAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_flash_auto_black_24dp));
                hideActiveBar();
            }
        });

        flashOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_flash_off_black_24dp));
                hideActiveBar();
            }
        });

        flashOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_flash_on_black_24dp));
                hideActiveBar();
            }
        });

    }

    public void hideActiveBar() {
        if (activeBar != null) {
            activeBar.animate()
                    .translationY(-activeBar.getHeight())
                    .setDuration(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            // don't remove this otherwise the animation wont't stick
                            activeBar.setVisibility(View.GONE);
                            ((Overlay) getActivity().findViewById(R.id.surfaceView)).drawGrid(cameraState.getGrid());
                        }
                    });
            setButtonsAlpha(1.0f);

        }
    }

    public LinearLayout getActiveBar() {
        return this.activeBar;
    }
}
