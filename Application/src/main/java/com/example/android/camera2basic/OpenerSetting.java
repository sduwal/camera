package com.example.android.camera2basic;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenerSetting extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="OpenerSetting";
    private static final int DATABASE_VERSION=1;

    public OpenerSetting(Context context){
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        updateDatabase(db,0,DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateDatabase(db,oldVersion,newVersion);
    }

    private static void insert(SQLiteDatabase db,int timer,int grid,int flash){
        ContentValues values=new ContentValues();
        values.put("TIMER", timer);
        values.put("GRID",grid);
        values.put("FLASH",flash);
        db.insert("OPENERSETTING",null,values);
    }
    private void updateDatabase(SQLiteDatabase db,int oldVersion,int newVersion){
        if(oldVersion<1){
            db.execSQL("CREATE TABLE OPENERSETTING (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    +"TIMER INTEGER,"
                    +"GRID INTEGER,"
                    +"FLASH INTEGER);");
            insert(db,0,0,2);
        }

        if(oldVersion<2){
            //if you need to update
        }
    }

}
