package com.example.android.camera2basic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TextureView;

public class Overlay extends TextureView {

    private final Paint paint;
    private final Context context;
    private FragmentActivity act;


    public Overlay(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOpaque(false);
        this.context = context;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        act = ((FragmentActivity)context);
    }

    public void darkenScreen() {
        invalidate();

        final Canvas canvas = lockCanvas();
        if (canvas != null) {
            canvas.drawColor(Color.BLACK);
            paint.setColor(Color.BLACK);
            canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
            unlockCanvasAndPost(canvas);
        }
        paint.setColor(Color.WHITE);
    }


    public void drawGrid(CameraState.Grid gridType) {
        int topY = act.findViewById(R.id.topBar).getHeight();
        int bottomOffset = act.findViewById(R.id.modeBar).getHeight() + act.findViewById(R.id.shutterBar).getHeight();
        int bottomY = getHeight() - bottomOffset;
        int w = getWidth();
        int h = getHeight() - topY - bottomOffset;

        invalidate();
        final Canvas canvas = lockCanvas();
//        Log.d("GRID", Boolean.toString(canvas == null));
        if (canvas != null) {
            paint.setStrokeWidth(1.0f);
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            if (gridType == CameraState.Grid.THIRD) {
                // vertical bars
                canvas.drawLine(w / 3, topY, w / 3, bottomY, paint);
                canvas.drawLine(2 * w / 3, topY, 2 * w / 3, bottomY, paint);
                // horizontal bars
                canvas.drawLine(0, topY + h / 3, getWidth(), topY + h / 3 , paint);
                canvas.drawLine(0, topY + 2 * h / 3, getWidth(), topY + 2 * h / 3,paint);
            } else if (gridType == CameraState.Grid.FOUR) {
                // vertical bars
                canvas.drawLine(w / 4, topY, w / 4, bottomY, paint);
                canvas.drawLine(2 * w / 4, topY, 2 * w / 4, bottomY, paint);
                canvas.drawLine(3 * w / 4, topY, 3 * w / 4, bottomY, paint);
                // horizontal bars
                canvas.drawLine(0, topY + h / 4, getWidth(), topY + h / 4 , paint);
                canvas.drawLine(0, topY + 2 * h / 4, getWidth(), topY + 2 * h / 4,paint);
                canvas.drawLine(0, topY + 3 * h / 4, getWidth(), topY + 3 * h / 4,paint);
            }
            unlockCanvasAndPost(canvas);

        }
    }

    public void clear() {
        invalidate();
        final Canvas canvas = lockCanvas();
        if (canvas != null) {
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            TopBar topBar= (TopBar)((act.getSupportFragmentManager().findFragmentById(R.id.topContainer)));
            topBar.hideActiveBar();

//            invalidate();
////            if (mHolder.getSurface().isValid()) {
//                final Canvas canvas = lockCanvas();
//                if (canvas != null) {
//                    paint.setStrokeWidth(4.0f);
////                    canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
//                    canvas.drawCircle(event.getX(), event.getY(), 150, paint);
//                    unlockCanvasAndPost(canvas);
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            Canvas canvas1 = lockCanvas();
//                            if(canvas1 !=null){
//                                canvas1.drawColor(0, PorterDuff.Mode.CLEAR);
//                                unlockCanvasAndPost(canvas1);
//                                drawGrid(((CameraActivity)context).getState().getGrid());
//                            }
//
//                        }
//                    }, 2000);
//
//                }
////            }
        }

        return false;
    }
}